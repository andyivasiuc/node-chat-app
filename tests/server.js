'use strict'

require('dotenv').config();

let assert = require('chai').assert;
let socket = require('socket.io-client')
let uuid = require('uid-safe');

let ssid = uuid.sync(18);

let operatorsIO = null;
let clientsIO = null;

describe('server', function(){

    it('user connected', function(d){

        operatorsIO = socket.connect(`http://${process.env.HOST}:${process.env.PORT}/operators?ssid=${ssid}`);

        operatorsIO.on('user-connected', function(data){
            assert.ok(data.ssid, 'User object has proppers ssid');
            d();
        });

        clientsIO = socket.connect(`http://${process.env.HOST}:${process.env.PORT}/clients?ssid=${ssid}`);

    });

    it('user message', function(d){

        operatorsIO.on('user-msg', function(msg){
            assert.equal(msg.ssid, ssid, 'Message was received with propper ssid');
            d();
        });

        clientsIO.emit('user-msg', {
            ssid: ssid,
            body: 'Lorem ipsum dolor.'
        });

    });

    it('user event', function(d){

        operatorsIO.on('user-event', function(event){
            assert.equal(event.ssid, ssid, 'Event was received with propper ssid');
            d();
        });

        clientsIO.emit('user-event', {
            ssid: ssid,
            action: 'idle'
        });

    });

    it('user disconnected', function(d){

        operatorsIO.on('user-disconnected', function(event){
            assert.equal(event.ssid, ssid, 'Event was received with propper ssid');
            d();
        });

        clientsIO.disconnect();

    });
});
