'use strict'

require('dotenv').config();

let assert = require('chai').assert;
let uuid = require('uid-safe');

import request from 'request';

let ssid = uuid.sync(18);

describe('Database tests', function(){

    it('flushing messages from test database', function(d){

        let reqUrl = `http://${process.env.DB_HOST}:${process.env.DB_TEST_PORT}/flush`;

        request({
            url: reqUrl,
            method: "GET"
        }, function(error, response, body){
            assert.equal(body, '{"status":"ok"}', 'Response has ok status');
            assert.isNull(error, 'Error is false');
            assert.equal(response.statusCode, 200, 'Server returns 200 code');
            d();
        });

    });

    it('getting empty response', function(d){

        let reqUrl = `http://${process.env.DB_HOST}:${process.env.DB_TEST_PORT}/messages/${ssid}`;

        request({
            url: reqUrl,
            method: "GET"
        }, function(error, response, body){
            assert.equal(body, '{"status":"ok","messages":[]}', 'Response is empty json object');
            assert.isNull(error, 'Error is false');
            assert.equal(response.statusCode, 200, 'Server returns 200 code');
            d();
        });

    });

    it('saving message to db', function(d){

        let reqUrl = `http://${process.env.DB_HOST}:${process.env.DB_TEST_PORT}/messages`;

        let msg_obj = {
            body: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            ssid: ssid,
            from: 'user',
            robot: false,
            date: new Date()
        };

        request({
            url: reqUrl,
            method: "POST",
            json: msg_obj
        }, function(error, response, body){
            assert.deepEqual(body, {status: 'ok'}, 'We get ok status in response');
            assert.isNull(error, 'Error is false');
            assert.equal(response.statusCode, 200, 'Server returns 200 code');
            d();
        });

    });

    it('checking saved message', function(d){

        let reqUrl = `http://${process.env.DB_HOST}:${process.env.DB_TEST_PORT}/messages/${ssid}`;

        request({
            url: reqUrl,
            method: "GET"
        }, function(error, response, body){
            let data = JSON.parse(body);
            let msg = data.messages[0];
            assert.equal(msg.ssid, ssid, 'Message from DB has propper ssid');
            assert.equal(msg.from, 'user', 'Message from DB has propper from field');
            assert.equal(msg.robot, false, 'Message from DB has propper robot field');
            assert.ok(!error, 'Error is false');
            assert.equal(response.statusCode, 200, 'Server returns 200 code');
            d();
        });

    });

});
