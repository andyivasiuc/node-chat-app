'use strict'

// Redis connection

let redis = require("redis"),
    redisClient = redis.createClient({
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        password: process.env.REDIS_PASSWORD
    }), multi;

// Пометить пользователя, что он оффлайн

export function markUserAsOnline(ssid){
    redisClient.setex(ssid, process.env.REDIS_EXPIRATION, 1);
}

// Пометить пользователя, что он онлайн

export function markUserAsOffline(ssid){
    redisClient.setex(ssid, process.env.REDIS_EXPIRATION, 0);
}

// Получить статусы пользователей пачкой

export function getUserStatuses(ssids, socket){

    let userStatus = {};
    let commands = [];

    // Проверяем, если мы получили массив

    if(Array.isArray(ssids)&&ssids.length){

        // Создаём массив комманд для отправки в Редис

        for(let ssid of ssids){
            commands.push(['get', ssid]);
        }

        // Делаем мульти-запрос к Редис

        redisClient.multi(commands).exec(function (err, replies) {

            if(!err){

                // Строим объект с информацией о пользователях
                // {ssid: [true/false], ...}

                for(let i in ssids){

                    let ssid = ssids[i];
                    let _reply = replies[i];
                    let reply = false;

                    if(_reply==='1'){
                        reply = true;
                    }

                    userStatus[ssid] = reply;

                }

                // Эмитим ивент обратно оператору

                socket.emit('bulk-user-status-response', userStatus);

            }

        });

    }
}
