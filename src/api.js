'use strict'

import request from 'request';

export function leadCreate(msg_obj, callback){

    let reqUrl = `http://${process.env.API_HOST}/api/v1/leads`;

    let reqObj = {
        question: msg_obj.body,
        ssid: msg_obj.ssid,
        page_domain: msg_obj.host,
        page_url: msg_obj.referer,
        referer: msg_obj.referer,
        status: "open",
        widget_id: msg_obj.widget_id
    };

    request({
        url: reqUrl,
        method: "POST",
        json: reqObj
    }, function(error, response, body){
        if (!error) {
            callback.call(null, body.lead_id);
        }else{

        }
    });

}

export function leadUpdate(obj){

    let reqUrl = `http://${process.env.API_HOST}/api/v1/leads/update`;

    request({
        url: reqUrl,
        method: "POST",
        json: obj
    }, function(error, response, body){
        if (!error && response.statusCode == 200) {

        }else{

        }
    });

}
