'use strict'

let uuid = require('uid-safe');
let url = require('url');

// Simple sessions socket.io middleware

function iosessions(socket, next) {

    // This method is used to add a new ssid attribute
    // to socket object, and to automaticaly
    // create new room identified by ssid,
    // same way as a new room is created for
    // each newly connected socket identified by socket id.
    // @SEE: http://socket.io/docs/rooms-and-namespaces/#default-room

    function apply_session_to_socket(socket, ssid){

        // Setting ssid value for socket object

        socket.ssid = ssid;

        // Adding socket to a room with ssid identifier,
        // so we can then broadcast to all sockets
        // of the same ssid.

        socket.join(ssid);
    }

    // Saving client host and referer

    const client_referer = socket.handshake.headers.referer;
    const client_host = url.parse(client_referer||'http://localhost').hostname;

    socket.host = client_host;
    socket.referer = client_referer;

    // Getting session cookie value

    const session_cookie_value = socket.handshake.query.ssid;

    // If session id is there

    if(session_cookie_value){

        let ssid = session_cookie_value;

        // Applying ssid to socket

        apply_session_to_socket(socket, ssid);

        next();

    // Otherwise create new ssid and send it to client

    }else{

        // Creating new ssid asynchronously

        uuid(18).then(function(ssid){

            // Applying ssid to socket

            apply_session_to_socket(socket, ssid);

            // Emiting a ssid obj to client to be stored in cookies

            socket.emit('ssid', {ssid});

            next();

        });

    }

}

export default iosessions;
