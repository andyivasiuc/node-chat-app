'use strict'

// ГЛАВНЫЙ ФАЙЛ ПРОЕКТА

// Подключаем модули

let express = require('express');
let http = require('http').Server(express());
let cookieParser = require('socket.io-cookie');
let io = require('socket.io')(http, { transports: ['websocket', 'polling'] });

// Подгружаем переменные окружения
// Переменные описаны в файле .env в корне проекта

require('dotenv').config();

// Подгружаем модуль отвечающий за сессии
// Каждому клиенту присваивается уникальный идентификатор
// Ssid - socket session id
// Он хранится в куки и помогает идентифицировать юзеров, когда они вернутся
// или откроют чат в другом окне

import iosessions from './session';

// Подгружаем модуль отвечающий за БД

import saveToDB from './db';

// Подгружаем модуль отвечающий за различные API запросы
// Например создание открытого лида

import {leadCreate, leadUpdate} from './api';

// Подгружаем методы контроля онлайн статуса юзеров

import {markUserAsOnline, markUserAsOffline, getUserStatuses} from './users';

// Редис адаптер для ноды. Необходимо раскоментировать, когда нужно будет
// масштабировать проект

//io.adapter(redis({
//     host: process.env.REDIS_HOST,
//     port: process.env.REDIS_PORT
// }));

/**
 *  КЛИЕНТСКИЙ КАНАЛ
 *  Здесь описана логика взаимодействия сервера с пользователями виджета.
 */

let clientsIO = io.of('/clients');

// Настраиваем middleware для socket.io

// Подулючаем мидлвэр для парсинга куки

clientsIO.use(cookieParser);

// Подключаем мидлвэр отвечающий за создание сессий

clientsIO.use(iosessions);

// Слушаем всех подключающихся пользователей

clientsIO.on('connection', function(socket){

    // У каждого пользователя свой уникальный объект сокета
    // На который мы дальше навешиваем оьработчики событий

    // Айди сокета
    let socket_id = socket.id;

    // Айди сессии сокета
    let socket_session_id = socket.ssid;

    let socket_host = socket.host;
    let socket_referer = socket.referer;

    // Создаём объект с основной инфой о текущей сессии

    let socket_session_obj = {
        id: socket_id,
        ssid: socket_session_id,
        host: socket_host,
        referer: socket_referer
    }

    // Отправляем сообщение, что пользователь подкоючился всем операторам
    // Которые слушают тот-же самый канал.
    // И пользователь и оператор должны быть подсоединены к одному и тому-же каналу(room)
    // Который идентифицируется ssid

    operatorsIO.to(socket_session_id).emit('user-connected', socket_session_obj);

    // Сохраняем онлайн статус пользователя

    markUserAsOnline(socket_session_id);

    // Событие получения сообщения от пользователя

    socket.on('user-msg', function(msg_obj){

        //let datetime = new Date();

        // Populating message object with creation date

        //msg_obj.datetime = datetime.toJSON();

        // Наполняем объект сообщения данными сессии

        msg_obj = Object.assign(msg_obj, socket_session_obj);

        // Updating msg object data

        msg_obj.from = 'user';

        // Отправляем сообщение в канал оператора

        operatorsIO.to(socket_session_id).emit('user-msg', msg_obj);

        // Сохраняем сообщение в базу

        saveToDB(msg_obj);

        // Делаем API запрос к Venyoo, чтобы создать открытый лид
        // Объект полученного сообщения должен содержать параметр
        // lead_create = true, чтобы лид был создан,
        // Иначе апи запрос не делается

        if(msg_obj.lead_create){

            // Апи запрос
            leadCreate(msg_obj, function(lead_id){

                msg_obj.id = lead_id;

                // Отправляем ивент о создании нового лида в CRM.
                statsIO.emit('lead', msg_obj);

            });

        }

        // Отметить пользователя, что он онлайн, раз уж он отправил сообщение
        markUserAsOnline(socket_session_id);

    });

    // При получении доп. информации о лиде пересылаем её оператору

    socket.on('user-info', function(obj){

        // Наполняем объект с инфой данными сессии
        obj = Object.assign(obj, socket_session_obj);

        // Отпровляем ивент с новыми данными
        operatorsIO.to(socket_session_id).emit('user-info', obj);

        // Апи запрос
        leadUpdate(obj);
    });

    // Передаём юзер ивент оператору(скролит, водит мышкой, печатает)

    socket.on('user-event', function(obj){

        // Отправляем ивент оператору
        operatorsIO.to(socket_session_id).emit('user-event', obj);

        // Отмечаем, что пользователь онлайн, раз он водит мышкой :)
        markUserAsOnline(socket_session_id);

    });

    // Передаём юзер ивент оператору(скролит, водит мышкой, печатает)

    socket.on('lead-update', function(obj){

        // Отправляем ивент оператору
        statsIO.emit('lead-update', obj);

        // Отмечаем, что пользователь онлайн, раз он водит мышкой :)
        leadUpdate(obj);

    });

    // Пользователь отключился

    socket.on('disconnect', function(){

        // Отправляем оператору ивент, что юзер отключился
        operatorsIO.to(socket_session_id).emit('user-disconnected', {
            id: socket_id,
            ssid: socket_session_id,
            host: socket_host
        });

        // Помечаем пользователя, что он оффлайн

        markUserAsOffline(socket_session_id);

    });

});

/**
 *  ОПЕРАТОРСКИЙ КАНАЛ
 */

let operatorsIO = io.of('/operators');

// Событие подулючения оператора

operatorsIO.on('connection', function(socket){

    // Получаем ssid оператора, он передаётся через GET параметр при подлючении
    // Оператора к серверу.

    let socket_session_id = socket.handshake.query.ssid;
    socket.ssid = socket_session_id;

    // Подлючаемся к каналу с соответствующим ssid

    socket.join(socket_session_id);

    // При получении сообщения от оператора

    socket.on('operator-msg', function(msg_obj){
        let datetime = new Date();
        msg_obj.datetime = datetime.toJSON();
        msg_obj.ssid = socket_session_id;
        msg_obj.from = 'operator';
        msg_obj.robot = false;

        // Отправляем пользователю сообщение
        clientsIO.to(socket_session_id).emit('operator-msg', msg_obj);

        // Сохраняем сообщение в БД
        saveToDB(msg_obj);
    });

    // Слушаем и перенаправляем событие показа контакт формы
    socket.on('contact-form', function(){
        clientsIO.to(socket_session_id).emit('contact-form');
    });

    // Ивент который подключает оператора к дополнительным каналам
    socket.on('join', function(room){
        socket.join(room);
    });

});


/**
 *  STATS NAMESPACE
 */

let statsIO = io.of('/stats');

statsIO.on('connection', function(socket){

    socket.on('bulk-user-status', function(ssids){
        getUserStatuses(ssids, socket);
    });

});

http.listen(process.env.PORT, process.env.HOST, function(){
  console.log(`listening on ${process.env.HOST}:${process.env.PORT}`);
});
